const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUI= require('swagger-ui-express')
const express = require('express')
const app = express()

const options = {
    definition : {
        openapi : '3.0.0',
        info : {
            title : 'Task Manager api',
            version : '1.0.0',
            description: "This api is for creating users and bus system reservaion created by Sagee Hamati and Antoine Abdallah.", 
            version: "1.0.0",
            title:" Bus System reservation", 
            contact: {
              email: "sagee.best1@gmail.com"
            }, 
        },
        servers:[{
            url: 'https://api-task-manager-ol.herokuapp.com',
            description: 'bus_system api'
        }
    ],
  },
    apis:['./doc/docs.yaml']
}

const swaggerDocs= swaggerJsDoc(options)

module.exports = {
    swaggerDocs,
    swaggerJsDoc,
    swaggerUI,
}