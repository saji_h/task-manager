const express = require('express')
require('./db/mongoose')
const swagger =require('../doc/swagger.js')
const userRouter = require('./routers/user')
const taskRouter = require('./routers/task')



const app = express()
const port = process.env.PORT


app.use(express.json())
app.use(userRouter)
app.use(taskRouter)
app.use('/api-docs',swagger.swaggerUI.serve,swagger.swaggerUI.setup(swagger.swaggerDocs))


app.listen(port, () => {
    console.log('Server is up on port ' + port)
})



