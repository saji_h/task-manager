const express = require('express')
const Task = require('../models/task')
const auth = require('../middleware/auth')
const router = new express.Router()

router.post('/tasks', auth, async (req, res) => {
    try {
        // const task = await new Task(req.body)
        const task = new Task({
            ...req.body,
            owner: req.user._id
        })
        await task.save()
        res.status(201).send(task)
    }
    catch (err) {
        res.status(400).send(err)
    }
})

router.get('/tasks/me', auth, async (req, res) => {

    const match = {}
    const sort = {}


    if (req.query.completed) {
        match.completed = req.query.completed === 'true'
    }

    if (req.query.sortBy) {
        const parts = req.query.sortBy.split(':')
        sort[parts[0]] = parts[1] === 'desc' ? -1 : 1

    }



    const user = req.user
    try {
        await user.populate({
            path: 'tasks',
            match,
            options: {
                limit: parseInt(req.query.limit),
                skip: parseInt(req.query.skip),
                sort

            }
        })
        res.send(user.tasks)
    }
    catch (error) {
        res.status(500).send(error)
    }
})


router.get('/tasks/me/:id', auth, async (req, res) => {
    const _id = req.params.id
    try {
        const task = await Task.findOne({ _id, owner: req.user._id })
        if (!task) {
          return  res.status(404).send()
        }
        res.status(200).send(task)
    }

    catch (e) {
        res.status(500).send()

    }
})

router.patch('/tasks/me/:id', auth, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdate = ['description', 'completed']
    const isValidUpdate = updates.every((update) => allowedUpdate.includes(update))
    if (!isValidUpdate) {
        res.status(400).send({ Error: 'Invalid update' })
    }
    try {
        const task = await Task.findOne({ _id: req.params.id, owner: req.user._id })

        if (!task) {
            return res.status(404).send()
        }
        else if (updates.length === 0) {
           return res.status(400).send({ Error: 'Can`t be empty' })
        }
        updates.forEach((update) => task[update] = req.body[update])
        await task.save()
        res.send(task)
    }

    catch (e) {
        res.status(400).send()
    }
})

router.delete('/tasks/me/:id', auth, async (req, res) => {

    try {
        const deletedTask = await Task.findByIdAndDelete({ _id: req.params.id, owner: req.user.id })

        if (!deletedTask) {
          return  res.status(404).send({ error: 'Task not found' })
        }

        res.send({deletedTask})

    } catch (error) {
        res.status(400).send(error)
    }
})

module.exports = router