const express = require('express')
const auth = require('../middleware/auth')
const User = require('../models/user')
const multer = require('multer')
const sharp = require('sharp')
const router = new express.Router()


router.post('/users/signup', async (req, res) => {
    const user = new User(req.body)

    try {
        await user.save()
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (e) {
        if (!(e.keyPattern === undefined)) {
                return res.status(409).send({ email: e.keyValue.email + ' is already registered' })
        }
        res.status(400).send(e)
    }
})

router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findBydCredentials(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.send({ user, token })
    } catch (e) {
        res.status(400).send(e)
    }
})

router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})

router.post('/users/logoutAll', auth, async (req, res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        res.send({ logoutALL: 'Logged out of all devices' })
    } catch (e) {
        res.status(500).send()
    }
})



router.get('/users/me', auth, async (req, res) => {
    res.send(req.user)
})



router.patch('/users/me', auth, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdate = ['name', 'email', 'password', 'age']
    const isValidUpdate = updates.every((update) => allowedUpdate.includes(update))

    if (!isValidUpdate) {
        return res.status(400).send({ Error: 'invalid updates!' })
    }

    try {

        updates.forEach((update) => req.user[update] = req.body[update])

        if (updates.length === 0) {
            return res.status(400).send({ Error: 'Fields cannot be empty' })
        }
        await req.user.save()
        res.send(req.user)

    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/users/me', auth, async (req, res) => {
    try {
        await req.user.remove()
        res.send({ DeletedUser: req.user })
    } catch (error) {
        res.status(500).send()
    }
})


const upload = multer({
    limits: {
        fileSize: 1000000,
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpeg|jpg|png)$/)) {
            cb(new Error('Please upload an image '))
        }

        cb(undefined, true)
    }

})

router.post('/users/me/profilePic', auth, upload.single('avatar'), async (req, res) => {
    const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()

    try {
        req.user.avatar = buffer
        await req.user.save()
        res.send({ status: 'Image uploaded' })
    } catch (error) {
        res.status(500).send(error)
    }
}, (error, req, res, next) => {
    res.status(400).send({ error: error.message })
})

router.get('/users/profilePic', async (req, res) => {
    try {
        const user = await User.findById(req.query.id)
        if (!user || !user.avatar) {
            throw ({ error: 'Not found' })
            // throw new Error()
        }
        res.set('Content-Type', 'image/png')
        res.send(user.avatar)

    } catch (e) {
        res.status(404).send(e)
    }

})

router.delete('/users/me/profilePic', auth, async (req, res) => {
    try {
        req.user.avatar = undefined
        await req.user.save()
        res.send({ deletedProfilePic: 'Profile picture deleted' })
    } catch (error) {
        res.status(500).send(error)
    }
})

module.exports = router